﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FanMatch.Models;

namespace FanMatch.Controllers
{

    [HackAuth]
    public class PersonController : Controller
    {
        public class PersonEditorViewModel
        {
            public Person Person;
            public IEnumerable<Fandom> AllFandoms;
        }

        private FanMatchDb db = new FanMatchDb();

        //
        // GET: /Person/

        public ViewResult Index()
        {
            return View(db.People.ToList());
        }

        //
        // GET: /Person/Details/5

        public ViewResult Details(int id)
        {
            Person person = db.People.Find(id);
            return View(person);
        }


        public ActionResult New(string name)
        {
            var person = new Person { Name = name };
            db.People.Add(person);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = person.Id });
        }

        public ViewResult DestroyAllHumans()
        {
            return View();
        }

        public ActionResult DestroyAllHumansConfirm()
        {
            var matches = db.Matches;
            foreach (var match in matches)
            {
                db.Matches.Remove(match);
            }

            var people = db.People;
            foreach (var person in people)
            {
                db.People.Remove(person);
            }

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        
        //
        // GET: /Person/Edit/5
 
        public ActionResult Edit(int id)
        {
            Person person = db.People.Find(id);
            
            var allFandoms = db.Fandoms.ToList();
            var model = new PersonEditorViewModel
            {
                Person = person,
                AllFandoms = allFandoms
            };
            return View(model);
        }

        public PartialViewResult _PersonTag(int id)
        {
            var person = db.People.Find(id);
            return PartialView(person);
        }

        public class PersonWebModel
        {
            public String Name { get; set; }
            public bool IsReader { get; set; }
            public bool IsWriter { get; set; }
            public bool CanMatchMultiple { get; set; }
            public List<InteractionPreference> InteractionPreferences { get; set; }
        }

        [HttpPost]
        public void Save(int id, PersonWebModel data)
        {
            var person = db.People.Find(id);

            person.Name = data.Name;
            person.IsReader = data.IsReader;
            person.IsWriter = data.IsWriter;
            person.CanMatchMultiple = data.CanMatchMultiple;
            person.SetInteractionPreference(data.InteractionPreferences ?? Enumerable.Empty<InteractionPreference>());

            var entry = db.Entry(person);
            entry.State = EntityState.Modified;

            db.SaveChanges();
        }


        //
        // GET: /Person/Delete/5
 
        public ActionResult Delete(int id)
        {
            Person person = db.People.Find(id);
            return View(person);
        }

        //
        // POST: /Person/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult AddFandom(int personId, int fandomId)
        {
            var addedFandom = db.Fandoms.Find(fandomId);
            AddFandomsToPerson(personId, new[] { addedFandom });

            return Json(new FandomInfo(addedFandom));
        }

        private void AddFandomsToPerson(int personId, IEnumerable<Fandom> addedFandoms)
        {
            var person = db.People.Find(personId);
            var entry = db.Entry(person);
            entry.State = EntityState.Modified;
            entry.Collection(e => e.Fandoms).Load();

            foreach (var fandom in addedFandoms)
            {
                person.Fandoms.Add(fandom);
            }


            db.SaveChanges();
        }

        [HttpPost]
        public JsonResult AddFandoms(int personId, IEnumerable<int> fandomIds)
        {
            var addedFandoms = db.Fandoms.Where(f => fandomIds.Contains(f.Id));
            AddFandomsToPerson(personId, addedFandoms);

            var addedFandomInfos = new List<FandomInfo>();
            foreach (var fandom in addedFandoms)
            {
                addedFandomInfos.Add(new FandomInfo(fandom));
            }

            return Json(addedFandomInfos);
        }

        [HttpPost]
        public void RemoveFandom(int personId, int fandomId)
        {
            var person = db.People.Find(personId);
            var entry = db.Entry(person);
            entry.State = EntityState.Modified;
            entry.Collection(e => e.Fandoms).Load();

            person.Fandoms = person.Fandoms.Where(f => f.Id != fandomId).ToList();

            db.SaveChanges();
        }

        [HttpPost]
        public JsonResult CreateFandomAndAddToPerson(string name, int personId)
        {
            var fandom = new Fandom { Name = name };
            db.Fandoms.Add(fandom);
            AddFandomsToPerson(personId, new[] { fandom });
            return Json(new FandomInfo(fandom));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }

    public class FandomInfo
    {
        public FandomInfo(Fandom addedFandom)
        {
            this.Id = addedFandom.Id;
            this.Name = addedFandom.Name;
            this.Population = addedFandom.People.Count();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Population { get; set; }
    }
}