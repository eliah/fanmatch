namespace FanMatch.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInteractionPreference : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "EncodedInteractionPreferences", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "EncodedInteractionPreferences");
        }
    }
}
