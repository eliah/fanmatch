﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FanMatch.Models.Entities;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity.ModelConfiguration;

namespace FanMatch.Models
{
    public class Person : IId
    {
        [Required]
        public string Name { get; set; }
        public int Id { get; set; }
        public virtual ICollection<Fandom> Fandoms { get; set; }
        private string EncodedInteractionPreferences { get; set; }

        public bool IsReader { get; set; }
        public bool IsWriter { get; set; }

        public bool CanMatchMultiple { get; set; }

        public bool Complements(Person other)
        {
            return ((IsReader && other.IsWriter)
                || (IsWriter && other.IsReader))
                && (other.Name != this.Name);
        }

        public override string ToString()
        {
            return String.Format("Person #{0}", Id);
        }

        public Person SetInteractionPreference(IEnumerable<InteractionPreference> preferences)
        {
            EncodedInteractionPreferences = String.Join(",", preferences.Select(p => (int)p));
            return this;
        }

        public ICollection<InteractionPreference> GetInteractionPreferences()
        {
            if (String.IsNullOrEmpty(EncodedInteractionPreferences))
            {
                return new List<InteractionPreference>();
            }
            else
            {
                return EncodedInteractionPreferences
                    .Split(',')
                    .Select(s => (InteractionPreference)Enum.Parse(typeof(InteractionPreference), s))
                    .ToList();
            }
        }

        public int InteractionPreferenceDistance(Person other)
        {
            var thisPreference = GetInteractionPreferences();
            var otherPreference = other.GetInteractionPreferences();

            if (!thisPreference.Any() || !otherPreference.Any())
            {
                return 5; // max
            }

            if (thisPreference.Intersect(otherPreference).Any())
            {
                return 0;
            }
            else
            {
                var thisMin = thisPreference.Min();
                var thisMax = thisPreference.Max();
                var otherMin = otherPreference.Min();
                var otherMax = otherPreference.Max();

                var leftDistance = Math.Abs(thisMin - otherMin);
                var rightDistance = Math.Abs(thisMax - otherMax);
                return Math.Min(leftDistance, rightDistance);
            }
        }

        public class Configuration : EntityTypeConfiguration<Person>
        {
            public Configuration()
            {
                Property(p => p.EncodedInteractionPreferences);
            }
        }
    }
}