﻿using System.Collections.Generic;
using FanMatch.Models.Entities;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace FanMatch.Models
{
    public class Fandom : IId
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ICollection<Person> People { get; set; }

        public string NormalizedName { get { return Normalize(Name); } }

        public static string Normalize(string input)
        {
            var nonAlphaNumeric = new Regex(@"[^A-Za-z0-9]");
            return nonAlphaNumeric.Replace(input, "");
        }
    }
}