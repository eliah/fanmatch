﻿var bootstrapData = window.FanMatchPersonData;
var Store = {
    Data: {
        Name: bootstrapData.Name,
        IsWriter: bootstrapData.IsWriter,
        IsReader: bootstrapData.IsReader,
        CanMatchMultiple: bootstrapData.CanMatchMultiple,
        Fandoms: bootstrapData.Fandoms,
        PersonId: bootstrapData.PersonId,
        PendingEntries: [],
        IP1: bootstrapData.IP1,
        IP2: bootstrapData.IP2,
        IP3: bootstrapData.IP3,
        IP4: bootstrapData.IP4,
        IP5: bootstrapData.IP5
    },

    AllFandoms: bootstrapData.AllFandoms,

    Persist: function () {
        var self = this;
        var interactionPreferences = [];
        if (self.Data.IP1) {
            console.log('found a pref')
            interactionPreferences.push(1);
        }
        if (self.Data.IP2) {
            console.log('found a pref')
            interactionPreferences.push(2);
        }
        if (self.Data.IP3) {
            console.log('found a pref')
            interactionPreferences.push(3);
        }
        if (self.Data.IP4) {
            console.log('found a pref')
            interactionPreferences.push(4);
        }
        if (self.Data.IP5) {
            console.log('found a pref')
            interactionPreferences.push(5);
        }
        $.post("/Person/Save/" + self.Data.PersonId,
            {
                Name: self.Data.Name,
                IsWriter: self.Data.IsWriter,
                IsReader: self.Data.IsReader,
                CanMatchMultiple: self.Data.CanMatchMultiple,
                InteractionPreferences: interactionPreferences
            }
        ).then(function () {
            self.trigger('update', self.Data);
        });
    },
    
    AddFandom: function (newFandom) {
        var self = this;
        self.Data.Fandoms.push(newFandom);

        self.trigger('update', self.Data);
    },

    normalizeFandomName: function(rawName) {
        var normalizerRegexp = /[^A-Za-z0-9]/;
        return rawName.trim().replace(normalizerRegexp, "").toLowerCase();
    },

    FindFandom: function (needle) {
        var self = this;
        var normalizedNeedle = self.normalizeFandomName(needle);
        return _.find(self.AllFandoms, function (candidate) {
            var normalizedCandidateName = self.normalizeFandomName(candidate.value);
            return normalizedCandidateName == normalizedNeedle;
        });
    },

    PersonHasFandom: function (fandom) {
        var self = this;
        var personFandomNames = _.pluck(self.Data.Fandoms, 'Name').map(self.normalizeFandomName);
        var normalizedCandidate = self.normalizeFandomName(fandom);
        return _.contains(personFandomNames, normalizedCandidate);
    },

    HandleError: function (jqXHR, textStatus, errorThrown) {
        alert("Error processing request: " + errorThrown);
    }
};

// ehtodo handle ajax errors
// ehtodo don't re-add dupe fandoms

MicroEvent.mixin(Store);

Store.bind('remove-pending-entry', function (entry) {
    var self = this;

    self.Data.PendingEntries = _.reject(self.Data.PendingEntries, function (pending) {
        return pending == entry;
    });

    self.trigger('update', self.Data);
});

Store.bind('update-name', function (name) {
    var self = this;
    self.Data.Name = name;
    self.Persist();
});

Store.bind('update-checkboxes', function (newCheckboxData) {
    var self = this;
    self.Data.IsWriter = newCheckboxData.IsWriter;
    self.Data.IsReader = newCheckboxData.IsReader;
    self.Data.CanMatchMultiple = newCheckboxData.CanMatchMultiple;
    self.Data.IP1 = newCheckboxData.IP1;
    self.Data.IP2 = newCheckboxData.IP2;
    self.Data.IP3 = newCheckboxData.IP3;
    self.Data.IP4 = newCheckboxData.IP4;
    self.Data.IP5 = newCheckboxData.IP5;
    self.Persist();
});

Store.bind('add-fandom', function (data) {
    var self = this;
    $.post(
        "/Person/AddFandom",
        { fandomId: data.fandomId, personId: self.Data.PersonId }
    ).done(function (newData) {
        self.AddFandom(newData);
    }).fail(Store.HandleError);
});


Store.bind('remove-fandom', function (data) {
    var self = this;
    $.post(
        "/Person/RemoveFandom",
        { fandomId: data.fandomId, personId: self.Data.PersonId }
    ).done(function () {
        self.Data.Fandoms = _.reject(self.Data.Fandoms, function (fandom) {
            return fandom.Id == data.fandomId;
        });
        self.trigger('update', self.Data)
    }).fail(Store.HandleError);
});

Store.bind('create-and-add-fandom', function (data) {
    var self = this;
    $.post("/Person/CreateFandomAndAddToPerson",
        { name: data.name, personId: self.Data.PersonId }
    ).done(function (newData) {
        self.AddFandom(newData);
    }).fail(Store.HandleError);
});

Store.bind('bulk-entry', function (fandoms) {
    var self = this;
    var fandomsToPersist = [];
    var unmatchedFandoms = [];
    _.each(fandoms, function (fandom) {
        if (!self.PersonHasFandom(fandom)) {
            var existingFandom = self.FindFandom(fandom);
            if (existingFandom) {
                console.debug("saving " + fandom);
                fandomsToPersist.push(existingFandom.id);
            } else {
                console.debug("couldn't match " + fandom);
                unmatchedFandoms.push(fandom);
            }
        }
    });

    if (fandomsToPersist.length > 0) {
        $.post(
            "/Person/AddFandoms",
            { personId: self.Data.PersonId, fandomIds: fandomsToPersist }
        ).done(function (data) {
            _.each(data, function (fandom) {
                self.Data.Fandoms.push(fandom);
            });
            self.trigger('update', self.Data);
        }).fail(Store.HandleError)
    }

    self.Data.PendingEntries = unmatchedFandoms;
    self.trigger('update', self.Data);
})

var Person = React.createClass({
    componentDidMount: function () {
        var self = this;
        Store.bind('update', function (data) {
            self.setState(data);
        });
    },
    getInitialState: function () {
        return Store.Data;
    },
    render: function () {
        return (
            <div>
                <NameField Name={this.state.Name} />
                <CheckBoxes IsWriter={this.state.IsWriter} IsReader={this.state.IsReader} CanMatchMultiple={this.state.CanMatchMultiple}
                    IP1={this.state.IP1} IP2={this.state.IP2} IP3={this.state.IP3} IP4={this.state.IP4} IP5={this.state.IP5} />
                <FandomList Fandoms={this.state.Fandoms} AllFandoms={this.props.AllFandoms} PendingEntries={this.state.PendingEntries} />
            </div>
        );
    }
});

var NameField = React.createClass({

    getInitialState: function() {
        return { Name: this.props.Name };
    },

    handleChange: function(e) {
        this.setState({ Name: e.target.value });
    },

    triggerPersist: function() {
        Store.trigger('update-name', this.state.Name);
    },

    render: function () {

        var saveButton = (this.state.Name == this.props.Name)
            ? ""
            : <button type="button" className="btn btn-default" value="Save" disabled={this.state.Name == this.props.Name} onClick={this.triggerPersist }>Save</button>;

        return (

            <div className="form-inline">
                <div className="form-group">
                    <label for="PersonName">Name:</label>
                    <input type="text" id="PersonName" className="form-control" defaultValue={this.props.Name} onChange={this.handleChange} />
                    {saveButton}
                </div>
            </div>
        );
    }
});

var CheckBoxes = React.createClass({

    handleChange: function () {
        var newData = {
            IsWriter: this.refs.IsWriter.checked,
            IsReader: this.refs.IsReader.checked,
            CanMatchMultiple: this.refs.CanMatchMultiple.checked,
            IP1: this.refs.IP1.checked,
            IP2: this.refs.IP2.checked,
            IP3: this.refs.IP3.checked,
            IP4: this.refs.IP4.checked,
            IP5: this.refs.IP5.checked,
        };

        Store.trigger("update-checkboxes", newData);
    },

    render: function () {
        return (
            <div>
                <div className="checkbox">
                    <label><input type="checkbox" ref="IsWriter" checked={this.props.IsWriter} onChange={this.handleChange} /> Is Writer</label>
                </div>
                <div className="checkbox">
                    <label><input type="checkbox" ref="IsReader" checked={this.props.IsReader} onChange={this.handleChange} /> Is Reader</label>
                </div>
                <div className="checkbox">
                    <label><input type="checkbox" ref="CanMatchMultiple" checked={this.props.CanMatchMultiple } onChange={this.handleChange}/> Can Match Multiple</label>
                </div>
                <div class="form-group">
                    <h3>Interaction Preference</h3>
                    <span className="checkbox">
                        <label><input type="checkbox" ref="IP1" checked={this.props.IP1} onChange={this.handleChange} />1</label>
                    </span>
                    <span className="checkbox">
                        <label><input type="checkbox" ref="IP2" checked={this.props.IP2} onChange={this.handleChange} />2</label>
                    </span>
                    <span className="checkbox">
                        <label><input type="checkbox" ref="IP3" checked={this.props.IP3} onChange={this.handleChange} />3</label>
                    </span>
                    <span className="checkbox">
                        <label><input type="checkbox" ref="IP4" checked={this.props.IP4} onChange={this.handleChange} />4</label>
                    </span>
                    <span className="checkbox">
                        <label><input type="checkbox" ref="IP5" checked={this.props.IP5} onChange={this.handleChange} />5</label>
                    </span>
                </div>
            </div>
        );
    }
});

var FandomList = React.createClass({

    getInitialState: function() {
        return {BulkEntry: ""};
    },

    handleBulkEntryInput: function(e) {
        this.setState({ BulkEntry: e.target.value });
    },

    processBulkEntry: function () {
        var entryData = this.refs.bulkEntry.value;
        var fandoms = entryData.split(",").map(function (s) { return s.trim(); });
        Store.trigger('bulk-entry', fandoms);
        this.setState({ BulkEntry: "" });
    },

    render: function () {
        var self = this;

        var fandomNodes = this.props.Fandoms.map(function (fandom) {
            return (
                <Fandom data={fandom} key={fandom.Name }/>
            );
        });

        var pendingEntryNodes = this.props.PendingEntries.map(function (fandomName) {
            return (
                <FandomEntry key={fandomName} AllFandoms={self.props.AllFandoms} PersonFandoms={self.props.Fandoms} Value={fandomName} />
            );
        });

        return (
            <div>
                <h3>Fandoms</h3>

                <h4>Bulk Entry:</h4>
                <div>
                    <div className="form-group">
                        <textarea className="form-control" ref="bulkEntry" value={this.state.BulkEntry} onChange={this.handleBulkEntryInput}></textarea>
                        <button className="btn btn-default" type="button" onClick={this.processBulkEntry}>Go</button>
                    </div>
                </div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Population</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        { fandomNodes }
                        { pendingEntryNodes }
                        <FandomEntry AllFandoms={this.props.AllFandoms} PersonFandoms={this.props.Fandoms} Value="" />
                    </tbody>

                </table>
            </div>
        );
    }
});

var FandomEntry = React.createClass({

    componentDidMount: function () {
        this.componentWillReceiveProps();
    },

    getInitialState: function() {
        return {value: this.props.Value};
    },

    componentWillReceiveProps: function() {
        //recalculate available fandoms for autocomplete
        var self = this;
        var presentFandomNames = _.pluck(self.props.PersonFandoms, 'Name');
        var fandomsNotAlreadyPresent = _.filter(self.props.AllFandoms, function (fandom) {
            return !_.contains(presentFandomNames, fandom.value);
        });
        $(self.refs.input).autocomplete({ source: fandomsNotAlreadyPresent, select: self.autocompleteSelected, autoFocus: true });
    },

    temporarilyIgnoreKeyDowns: function() {
        var self = this;
        self.ignoreKeyDown = true;
        setTimeout(function() {self.ignoreKeyDown = false;}, 500);
    },

    autocompleteSelected: function(e, ui) {
        this.temporarilyIgnoreKeyDowns();
        Store.trigger("remove-pending-entry", this.props.Value);
        Store.trigger("add-fandom", { fandomId: ui.item.id });
        this.setState({value: ""});
    },

    handleChange: function(e) {
        this.setState({ value: e.target.value });
    },

    keyDown: function(e) {
        if (e.keyCode == 13 && !this.ignoreKeyDown) {
            Store.trigger("remove-pending-entry", this.props.Value);
            Store.trigger("create-and-add-fandom", { name: this.state.value });
            this.setState({value: ""});
        }
    },

    render: function () {
        return(
            <tr>
                <td>
                    <input ref="input" 
                           type="text" 
                           value={this.state.value} 
                           onChange={this.handleChange} 
                           onKeyDown={this.keyDown} />
                </td>
                <td></td>
                <td></td>
            </tr>
        )
    }
})

var Fandom = React.createClass({
    delete: function (e) {
        e.preventDefault();
        Store.trigger("remove-fandom", { fandomId: this.props.data.Id });
    },

    render: function () {
        return (
            <tr ref="tr">
                <td><a href={"/Fandom/Details/"+this.props.data.Id}>{this.props.data.Name}</a></td>
                <td>{this.props.data.Population}</td>
                <td><a href="#" onClick={this.delete}>Remove</a></td>
            </tr>
        );
    }
});

ReactDOM.render(
    <Person AllFandoms={Store.AllFandoms} />,
    document.getElementById('ReactContent')
);