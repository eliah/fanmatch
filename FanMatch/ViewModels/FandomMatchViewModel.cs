﻿using FanMatch.Models;

namespace FanMatch.ViewModels
{
    public class FandomMatch
    {
        public string Input { get; set; }
        public SimpleFandomModel MatchingExistingFandom { get; set; }
    }
}