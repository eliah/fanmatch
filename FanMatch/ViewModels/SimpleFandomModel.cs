﻿using FanMatch.Models;

namespace FanMatch.ViewModels
{
    public class SimpleFandomModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public SimpleFandomModel(Fandom fandom)
        {
            Id = fandom.Id;
            Name = fandom.Name;
        }
    }
}