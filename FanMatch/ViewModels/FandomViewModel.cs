﻿using FanMatch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FanMatch.ViewModels
{
    public class FandomViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<Person> People { get; set; }

        public FandomViewModel()
        {
            this.People = new List<Person>();
        }

        public FandomViewModel(Fandom fandom)
        {
            this.Id = fandom.Id;
            this.Name = fandom.Name;
            this.People = fandom.People.ToList();
        }

        public bool HasMatchAvailable()
        {
            var readers = People.Where(p => p.IsReader).ToList();
            var writers = People.Where(p => p.IsWriter).ToList();

            foreach (var reader in readers)
            {
                if (writers.Any(w => w.Id != reader.Id))
                {
                    return true;
                }
            }

            return false;
        }
    }
}