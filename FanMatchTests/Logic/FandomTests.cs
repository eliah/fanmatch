﻿using FanMatch.Models;
using NUnit.Framework;

namespace FanMatchTests.Logic
{
    class FandomTests
    {
        [Test]
        public void NormalizeWorksAsExpected()
        {
            var input = "abc[123!f";
            var output = Fandom.Normalize(input);
            Assert.AreEqual("abc123f", output);
        }
    }
}
