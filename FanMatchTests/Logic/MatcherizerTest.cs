﻿using FanMatch.Models;
using FanMatch.Models.Logic;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FanMatchTests.Logic
{
    public class MatcherizerTest
    {
        private IdGenerator idGen = new IdGenerator();
        private List<Person> people;
        private List<Match> existingMatches;

        [SetUp]
        public void Setup()
        {
            this.people = new List<Person>();
            this.universalFandom = MakeFandom();
            this.existingMatches = new List<Match>();
        }

        private Person MakePerson(ICollection<Fandom> fandoms = null, bool reader = false, bool writer = false,
            string name = null, bool canMatchMultiple = false)
        {
            var p = new Person
            {
                Fandoms = fandoms ?? new[] { this.universalFandom },
                IsReader = reader,
                IsWriter = writer,
                Id = idGen.GetId(),
                Name = name ?? Guid.NewGuid().ToString(),
                CanMatchMultiple = canMatchMultiple
            };
            this.people.Add(p);
            return p;
        }

        private Fandom universalFandom;

        private Fandom MakeFandom()
        {
            return new Fandom { Id = this.idGen.GetId() };
        }

        private Person MakeReader(ICollection<Fandom> fandom = null, bool canMatchMultiple = false)
        {
            return this.MakePerson(fandom, reader: true, canMatchMultiple: canMatchMultiple);
        }

        private Person MakeWriter(ICollection<Fandom> fandom = null, bool canMatchMultiple = false)
        {
            return this.MakePerson(fandom, writer: true, canMatchMultiple: canMatchMultiple);
        }

        private void MakeWriters(int n)
        {
            for (var i = 0; i < n; i++)
            {
                this.MakeWriter();
            }
        }

        private MatchResult Match(params Person[] people)
        {
            IEnumerable<Person> matchPeople = people;
            if (!people.Any())
            {
                matchPeople = this.people;
            }
            return new Matcherizer(matchPeople, existingMatches).Matcherize();
        }

        [Test]
        public void PerfectPairingIsMatched()
        {
            var fandom = new[] { MakeFandom() };
            var reader = this.MakePerson(fandom, reader: true);
            var writer = this.MakePerson(fandom, writer: true);

            var matchInfo = new Matcherizer(new[] { reader, writer }, existingMatches).Matcherize();
            var matches = matchInfo.Matches;

            Assert.That(matches.Count(), Is.EqualTo(1), "There should be exactly 1 match");
            var match = matches.Single();
            Assert.That(match.Reader, Is.EqualTo(reader), "The reader of the match should be our reader person");
            Assert.That(match.Writer, Is.EqualTo(writer), "The writer of the match should be our writer person");
            Assert.That(matchInfo.UnmatchedPeople, Is.Empty, "There should be no unmatched people");
        }

        [Test]
        public void PeopleWithSameName_AreNotMatched()
        {
            var fandom = new[] { MakeFandom() };
            const string name = "Some Person";
            var person1 = MakePerson(fandom, reader: true, name: name);
            var person2 = MakePerson(fandom, writer: true, name: name);

            var matches = new Matcherizer(new[] { person1, person2 }, existingMatches).Matcherize().Matches;

            Assert.That(matches, Is.Empty,
                "Two people with the same name should not match");
        }

        [Test]
        public void UnmatchedPeopleAreUnmatched()
        {
            var popularFandom = new[] { MakeFandom() };
            var loneliestFandom = new[] { MakeFandom() };
            var personA = MakePerson(popularFandom, reader: true);
            var personB = MakePerson(popularFandom, writer: true);
            var personC = MakePerson(loneliestFandom, reader: true, writer: true);

            var res = this.Match(personA, personB, personC);

            Assert.That(res.Matches.Count(), Is.EqualTo(1), "There should be 1 match");
            Assert.That(res.UnmatchedPeople.Count(), Is.EqualTo(1), "There should be exactly 1 unmatched person");
            Assert.That(res.UnmatchedPeople.Single(), Is.EqualTo(personC),
                "Person C should be unmatched because no one else is in their fandom");
        }

        [Test]
        public void MultipleMatchPeopleCanMatchMultipleTimes()
        {
            var mx = MakeReader();
            mx.CanMatchMultiple = true;
            MakeWriter();
            MakeWriter();

            var matches = this.Match().Matches;

            Assert.That(matches.Count(m => m.Reader == mx), Is.EqualTo(2), "There should be 2 matches for the multi-match person");
        }

        private void AssertMatch(ICollection<Match> matches, Person a, Person b, string message)
        {
            Assert.That(matches.Any(m => m.Reader == a && m.Writer == b || m.Writer == a && m.Reader == b),
                message);
        }

        private void AssertMatch(ICollection<Match> matches, Person a, string message)
        {
            Assert.That(matches.Any(m => m.Reader == a || m.Writer == a),
                message);
        }

        [Test]
        public void EveryoneGetsOneMatchBeforeWeLookForDoubleMatches()
        {
            var smallFandom = MakeFandom();
            var largeFandom = MakeFandom();

            AddExistingMatchesForFandom(largeFandom, 10);

            var mx = MakeReader(fandom: new[] { smallFandom });
            mx.CanMatchMultiple = true;
            existingMatches.Add(new Match { Reader = mx, Writer = MakeWriter(), IsLocked = true });

            var single = MakeReader(fandom: new[] { largeFandom });

            MakeWriter(new[] { smallFandom, largeFandom });

            var matches = this.Match().Matches;

            AssertMatch(matches, single, "The non-mx person should have a match");
        }


        [Test]
        public void NoMatchPeopleWillMatchWithMatchedPeopleBeforeMatchedPeopleMatchWithEachOther()
        {
            var smallFandom = MakeFandom();
            var largeFandom = MakeFandom();

            /* A and B are locked (reader/writer)
             * C and D are locked (reader/writer)
             * E is unmatched reader compatible with D on LF
             * but A and D are mx-match and are compatible on a smaller fandom
             * 
             * Ensure that E has a match
             */

            var bothFandoms = new[] { smallFandom, largeFandom };
            var a = MakeReader(bothFandoms, canMatchMultiple: true);
            var b = MakeWriter(bothFandoms, canMatchMultiple: true);
            var c = MakeReader(bothFandoms, canMatchMultiple: true);
            var d = MakeWriter(bothFandoms, canMatchMultiple: true);
            var e = MakeReader(new[] { largeFandom });

            AddExistingMatchesForFandom(largeFandom, 10);

            Lock(a, b);
            Lock(c, d);

            var matchResult = this.Match();
            var matches = matchResult.Matches;

            AssertMatch(matches, e, "Person E should have a match");
            CollectionAssert.IsEmpty(matchResult.UnmatchedPeople);
        }

        [Test]
        public void PreferMatchesWithGreaterNumberOfFandoms()
        {
            var fandomA = MakeFandom();
            var fandomB = MakeFandom();

            var amethyst = MakeReader(new[] { fandomA, fandomB });
            var pearl = MakeWriter(new[] { fandomA });
            var garnet = MakeWriter(new[] { fandomA, fandomB });

            var matches = Match().Matches;
            AssertMatched(matches, amethyst, garnet);
        }

        [Test]
        public void PreferMatchesWithOverlappingInteractionPreference()
        {
            var fandom1 = MakeFandom();
            var fandom2 = MakeFandom();

            var nissa = MakeReader(new[] { fandom1, fandom2 }).SetInteractionPreference(
                new[] { InteractionPreference.Four });
            var chandra = MakeWriter(new[] { fandom1 }).SetInteractionPreference(
                new[] { InteractionPreference.Four, InteractionPreference.Five });
            var jace = MakeWriter(new[] { fandom1, fandom2 }).SetInteractionPreference(
                new[] { InteractionPreference.One });

            var matches = Match().Matches;
            AssertMatched(matches, chandra, nissa);
        }

        private void AddExistingMatchesForFandom(Fandom f, int numberOfMatches)
        {
            for (int i = 0; i < numberOfMatches; i++)
            {
                var reader = MakeReader(new[] { f });
                var writer = MakeWriter(new[] { f });
                var match = new Match { Reader = reader, Writer = writer, IsLocked = true };
                existingMatches.Add(match);
            }
        }

        [Test]
        public void SingleMatchPeopleAreOnlyMatchedOnce()
        {
            MakeReader();
            MakeWriter();
            MakeWriter();

            var matches = this.Match().Matches;

            Assert.That(matches.Count(), Is.EqualTo(1), "There should only be one match for a single-match person");
        }

        [Test]
        public void MultiplePeopleObeyTheMatchLimit()
        {
            MakeReader().CanMatchMultiple = true;
            MakeWriters(Matcherizer.MAX_MATCHES_PER_PERSON + 1);

            var matches = this.Match().Matches;

            Assert.That(matches.Count, Is.EqualTo(Matcherizer.MAX_MATCHES_PER_PERSON),
                "The maximum match limit should be obeyed");

        }

        [Test]
        public void YouCanOnlyMatchIfYouShareAFandom()
        {
            var fandomA = new[] { MakeFandom() };
            var fandomB = new[] { MakeFandom() };
            MakeReader(fandomA);
            MakeWriter(fandomB);

            var matches = this.Match().Matches;

            Assert.That(matches, Is.Empty, "There should be no matches because the people don't share a fandom");
        }

        [Test]
        public void MultiMatchPeopleAreNotUnmatchedIfTheyHaveOneMatch()
        {
            var fandomA = MakeFandom();
            var fandomB = MakeFandom();
            MakeReader(new[] { fandomA, fandomB }).CanMatchMultiple = true;
            MakeWriter(new[] { fandomA });

            var matchi = this.Match();

            Assert.That(matchi.Matches.Count, Is.EqualTo(1), "There should be exactly one match");
            Assert.That(matchi.UnmatchedPeople, Is.Empty, "There should be no unmatched people because everyone has a match");
        }

        private void Ban(Person reader, Person writer)
        {
            this.existingMatches.Add(new Match { IsBanned = true, Reader = reader, Writer = writer });
        }

        private void Lock(Person reader, Person writer)
        {
            this.existingMatches.Add(new Match { IsLocked = true, Reader = reader, Writer = writer });
        }

        [Test]
        public void BannedMatchesAreNotAllowed()
        {
            var reader = MakeReader();
            var writer = MakeWriter();
            this.Ban(reader, writer);

            var matchi = this.Match();

            Assert.That(matchi.Matches, Is.Empty, "There should be no matches because all of them are banned");
            Assert.That(matchi.UnmatchedPeople.Count, Is.EqualTo(2), "Both people should be unmatched");
            Assert.That(matchi.BannedMatches.Count, Is.EqualTo(1), "The banned match should come through");
        }

        [Test]
        public void LockedMatchesAreForced()
        {
            var fandomA = MakeFandom();
            var fandomB = MakeFandom();
            var r = MakeReader(new[] { fandomA });
            var w = MakeWriter(new[] { fandomB });
            this.Lock(r, w);

            var matchi = this.Match();

            Assert.That(matchi.LockedMatches.Count, Is.EqualTo(1), "There should be a match despite the fandom mismatch because it is locked");
            Assert.That(matchi.UnmatchedPeople, Is.Empty, "There should be no unmatched people");
        }

        [Test]
        public void MultiMatchersWontMatchEachOtherMultiply()
        {
            MakePerson(reader: true, writer: true).CanMatchMultiple = true;
            MakePerson(reader: true, writer: true).CanMatchMultiple = true;

            var matches = this.Match().Matches;

            Assert.That(matches.Count, Is.EqualTo(1), "There should still only be 1 match because there are only 2 people");
        }

        [Test]
        public void LockedPersonCanMatchAgain()
        {
            var a = MakeReader();
            a.CanMatchMultiple = true;
            var b = MakeWriter();
            var c = MakeWriter();
            this.Lock(a, b);

            var matches = this.Match();

            Assert.That(matches.Matches.Count, Is.EqualTo(1), "There should be one on-the-fly match");
            Assert.That(matches.LockedMatches.Count, Is.EqualTo(1), "There should be one locked match");
        }

        private void AssertMatched(ICollection<FanMatch.Models.Match> matches, Person personA, Person personB)
        {
            foreach (var match in matches)
            {
                if (match.Reader == personA && match.Writer == personB
                    || match.Reader == personB && match.Writer == personA)
                {
                    return;
                }
            }

            Assert.Fail("Expected to find a match containing {0} and {1}", personA, personB);
        }

        [Test]
        public void PersonWithNoFandoms_DoesNotCrashMatcherizer()
        {
            var person = MakePerson(fandoms: new Fandom[0]);

            Match();
        }

        //ehtodo test for person with 0 fandoms
    }
}
