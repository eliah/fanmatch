﻿using FanMatch.Models;
using NUnit.Framework;

namespace FanMatchTests.Models
{
    class PersonTest
    {
        [Test]
        public void DistanceIsFiveIfNothingSet()
        {
            Person personA = new Person().SetInteractionPreference(new[] { InteractionPreference.Three });
            Person personB = new Person();

            Assert.AreEqual(5, personA.InteractionPreferenceDistance(personB));
        }

        [Test]
        public void CommuncationDistanceIsZeroIfOverlap()
        {
            Person personA = new Person().SetInteractionPreference(new[] { InteractionPreference.Five });
            Person personB = new Person().SetInteractionPreference(new[] { InteractionPreference.Five });

            Assert.AreEqual(0, personA.InteractionPreferenceDistance(personB));
        }

        [Test]
        public void CommDistanceIfBelow()
        {
            Person personA = new Person().SetInteractionPreference(new[] { InteractionPreference.Four, InteractionPreference.Five });
            Person personB = new Person().SetInteractionPreference(new[] { InteractionPreference.One });

            Assert.AreEqual(3, personA.InteractionPreferenceDistance(personB));
        }
    }
}
